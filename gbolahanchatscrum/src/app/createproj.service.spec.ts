import { TestBed } from '@angular/core/testing';

import { CreateprojService } from './createproj.service';

describe('CreateprojService', () => {
  let service: CreateprojService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateprojService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
