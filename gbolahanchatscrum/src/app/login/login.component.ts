import { Component, OnInit } from '@angular/core';
import { Scrumuser } from '../scrumuser';
import {ScrumdataService } from '../scrumdata.service';
import { Router } from '@angular/router';

let feedback='';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  feedback ='';
  
  scrumUserLoginData = new Scrumuser('', '', '', '', '');
  constructor(private _scrumdataService: ScrumdataService, private _router: Router) { }

  ngOnInit(){}
  
  onLoginSubmit() {
     this._scrumdataService.login(this.scrumUserLoginData).subscribe(
      data => {console.log('Success!! Login Accepted ', data)
      localStorage.setItem('Authuser', JSON.stringify(this.scrumUserLoginData));
      localStorage.setItem('Authobj', JSON.stringify(data));
      localStorage.setItem('token', data.token);
      this.feedback = 'Login successful'
      this._router.navigate(['/scrumboard', data['project_id']])
    },
      
       error => {console.log('Error in Login!!!', error)
       this.feedback = 'Invalid Credentials'}
     )   
  }

}
